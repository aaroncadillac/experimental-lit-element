import {html, LitElement} from 'lit-element';

class ExperimentElement extends LitElement {
  
  static counterChanged (newValue, oldValue) {
    console.log(newValue,oldValue)
    return newValue;
  }

  static get properties() {
    return {
      counter: {
        type: Number,
        hasChanged: this.counterChanged
      },
      text: {type: String},
      color: {type: String}
    }
  }

  constructor() {
    super();

    this.counter = 0;
    this.text = '';
  }

  plusCounter() {
    this.counter++;
  }
  render() {
    return html`
      <style>
        .color {
          color: ${this.color};
        }
      </style>
      <h1>${this.text}</h1>
      <button @click="${this.plusCounter}">Haz click!</button>
      <p class="color">El valor del contador es: ${this.counter}</p>
    `;
  }  
}

customElements.define('experiment-element', ExperimentElement);
